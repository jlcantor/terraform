terraform{
	backend "s3"{
		bucket = "ea-terraform-training-lab"
		key = "ecs/terraform.tfstate"
		region = "us-west-1"
	}
}
